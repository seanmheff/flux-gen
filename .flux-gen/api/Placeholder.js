var API = '/<<<placeholder>>>/';
var Constants = require('../constants/Constants');
var RequestHelper = require('../util/RequestHelper');
var Dispatcher = require('../dispatcher/Dispatcher');
var Action = require('../actions/<<<Placeholder>>>ServerActionCreators');

var CREATE_ERROR = 'There was an error creating the <<<placeholder>>>';
var UPDATE_ERROR = 'There was an error updating the <<<placeholder>>>';
var DELETE_ERROR = 'There was an error deleting the <<<placeholder>>>';

function _apiRequestStarting(requestType) {
  Dispatcher.dispatch({
    type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_STARTING,
    requestType: requestType
  });
}

function _apiRequestDone(requestType) {
  Dispatcher.dispatch({
    type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
    requestType: requestType
  });
}

module.exports = {

  getAll: function() {
    _apiRequestStarting('getAll');
    return RequestHelper.getJSON(API)
      .then(function(<<<placeHolder>>>s) { Action.receive<<<Placeholder>>>s(<<<placeHolder>>>s); })
      .catch(function(error) { Action.receive<<<Placeholder>>>sError(error); })
      .then(function() { _apiRequestDone('getAll'); });
  },

  create: function(<<<placeHolder>>>) {
    _apiRequestStarting('create');
    return RequestHelper.postJSON(API, <<<placeholder>>>)
      .then(function(<<<placeHolder>>>) { Action.create<<<Placeholder>>>(<<<placeHolder>>>[0]); })
      .catch(function(error) { Action.create<<<Placeholder>>>Error(CREATE_ERROR); })
      .then(function() { _apiRequestDone('create'); });
  },

  update: function(<<<placeHolder>>>) {
    _apiRequestStarting('update');
    return RequestHelper.putJSON(API + <<<placeholder>>>.id, <<<placeholder>>>)
      .then(function(<<<placeHolder>>>) { Action.update<<<Placeholder>>>(<<<placeHolder>>>[0]); })
      .catch(function(error) { Action.update<<<Placeholder>>>Error(UPDATE_ERROR); })
      .then(function() { _apiRequestDone('update'); });
  },

  delete: function(<<<placeHolder>>>) {
    _apiRequestStarting('delete');
    return RequestHelper.delete(API + <<<placeholder>>>.id)
      .then(function() { Action.delete<<<Placeholder>>>(<<<placeHolder>>>.id); })
      .catch(function(error) { Action.delete<<<Placeholder>>>Error(DELETE_ERROR); })
      .then(function() { _apiRequestDone('delete'); });
  }

};
