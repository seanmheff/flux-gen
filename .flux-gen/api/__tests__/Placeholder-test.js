'use strict';

jest.dontMock('../<<<Placeholder>>>');

describe('<<<Placeholder>>> API', function() {
  var error = { message: 'error' };
  var <<<placeHolder>>>s = [ { id: 1, name: 'sean' } ];
  var API = require('../<<<Placeholder>>>');
  var Constants = require('../../constants/Constants');
  var Dispatcher = require('../../dispatcher/Dispatcher');
  var RequestHelper = require('../../util/RequestHelper');
  var ActionCreator = require('../../actions/<<<Placeholder>>>ServerActionCreators');

  describe('getAll', function(done) {
    it('calls receive<<<Placeholder>>>s action on API success', function(done) {
      RequestHelper.getJSON.mockReturnValue(Promise.resolve(<<<placeHolder>>>s));
      API.getAll().then(function() {
        expect(ActionCreator.receive<<<Placeholder>>>s).toBeCalledWith(<<<placeHolder>>>s);
        done();
      });
    });

    it('calls receive<<<Placeholder>>>sError action on API failure', function(done) {
      RequestHelper.getJSON.mockReturnValue(Promise.reject(error));
      API.getAll().then(function() {
        expect(ActionCreator.receive<<<Placeholder>>>sError).toBeCalledWith(error);
        done();
      });
    });

    it('dispatches a "getAll starting" event', function() {
      API.getAll();
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_STARTING,
        requestType: 'getAll'
      });
    });

    it('dispatches a "getAll done" event when API request succeeds', function(done) {
      RequestHelper.getJSON.mockReturnValue(Promise.resolve(<<<placeHolder>>>s));
      API.getAll().then(function() {
        expect(Dispatcher.dispatch).toBeCalledWith({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'getAll'
        });
        done();
      });
    });

    it('dispatches a "getAll done" event when API request fails', function(done) {
      RequestHelper.getJSON.mockReturnValue(Promise.reject(error));
      API.getAll().then(function() {
        expect(Dispatcher.dispatch).toBeCalledWith({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'getAll'
        });
        done();
      });
    });
  });

  describe('create', function(done){
    it('calls create<<<Placeholder>>> action on API success', function(done) {
      RequestHelper.postJSON.mockReturnValue(Promise.resolve(<<<placeHolder>>>s));
      API.create(<<<placeHolder>>>s[0]).then(function() {
        expect(ActionCreator.create<<<Placeholder>>>).toBeCalledWith(<<<placeHolder>>>s[0]);
        done();
      });
    });

    it('calls create<<<Placeholder>>>Error action on API failure', function(done) {
      RequestHelper.postJSON.mockReturnValue(Promise.reject());
      API.create(<<<placeHolder>>>s[0]).then(function() {
        expect(ActionCreator.create<<<Placeholder>>>Error)
          .toBeCalledWith('There was an error creating the <<<placeholder>>>');
        done();
      });
    });

    it('dispatches a "create starting" event', function() {
      API.create();
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_STARTING,
        requestType: 'create'
      });
    });

    it('dispatches a "create done" event when API request succeeds', function(done) {
      RequestHelper.postJSON.mockReturnValue(Promise.resolve(<<<placeHolder>>>s));
      API.create(<<<placeHolder>>>s[0]).then(function() {
        expect(Dispatcher.dispatch).toBeCalledWith({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'create'
        });
        done();
      });
    });

    it('dispatches a "create done" event when API request fails', function(done) {
      RequestHelper.postJSON.mockReturnValue(Promise.reject(error));
      API.create(<<<placeHolder>>>s[0]).then(function() {
        expect(Dispatcher.dispatch).toBeCalledWith({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'create'
        });
        done();
      });
    });
  });

  describe('update', function(done){
    it('calls update<<<Placeholder>>> action on API success', function(done) {
      RequestHelper.putJSON.mockReturnValue(Promise.resolve(<<<placeHolder>>>s));
      API.update(<<<placeHolder>>>s[0]).then(function() {
        expect(ActionCreator.update<<<Placeholder>>>).toBeCalledWith(<<<placeHolder>>>s[0]);
        done();
      });
    });

    it('calls update<<<Placeholder>>>Error action on API failure', function(done) {
      RequestHelper.putJSON.mockReturnValue(Promise.reject());
      API.update(<<<placeHolder>>>s[0]).then(function() {
        expect(ActionCreator.update<<<Placeholder>>>Error)
          .toBeCalledWith('There was an error updating the <<<placeholder>>>');
        done();
      });
    });

    it('dispatches a "update starting" event', function() {
      API.update(<<<placeHolder>>>s[0]);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_STARTING,
        requestType: 'update'
      });
    });

    it('dispatches a "update done" event when API request succeeds', function(done) {
      RequestHelper.putJSON.mockReturnValue(Promise.resolve(<<<placeHolder>>>s));
      API.update(<<<placeHolder>>>s[0]).then(function() {
        expect(Dispatcher.dispatch).toBeCalledWith({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'update'
        });
        done();
      });
    });

    it('dispatches a "update done" event when API request fails', function(done) {
      RequestHelper.putJSON.mockReturnValue(Promise.reject(error));
      API.update(<<<placeHolder>>>s[0]).then(function() {
        expect(Dispatcher.dispatch).toBeCalledWith({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'update'
        });
        done();
      });
    });
  });

  describe('delete', function(done){
    it('calls delete<<<Placeholder>>> action on API success', function(done) {
      RequestHelper.delete.mockReturnValue(Promise.resolve(<<<placeHolder>>>s[0]));
      API.delete(<<<placeHolder>>>s[0]).then(function() {
        expect(ActionCreator.delete<<<Placeholder>>>).toBeCalledWith(<<<placeHolder>>>s[0].id);
        done();
      });
    });

    it('calls delete<<<Placeholder>>>Error action on API failure', function(done) {
      RequestHelper.delete.mockReturnValue(Promise.reject());
      API.delete(<<<placeHolder>>>s[0]).then(function() {
        expect(ActionCreator.delete<<<Placeholder>>>Error)
          .toBeCalledWith('There was an error deleting the <<<placeholder>>>');
        done();
      });
    });

    it('dispatches a "delete starting" event', function() {
      API.delete(<<<placeHolder>>>s[0]);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_STARTING,
        requestType: 'delete'
      });
    });

    it('dispatches a "delete done" event when API request succeeds', function(done) {
      RequestHelper.deleteJSON.mockReturnValue(Promise.resolve(<<<placeHolder>>>s));
      API.delete(<<<placeHolder>>>s[0]).then(function() {
        expect(Dispatcher.dispatch).toBeCalledWith({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'delete'
        });
        done();
      });
    });

    it('dispatches a "delete done" event when API request fails', function(done) {
      RequestHelper.deleteJSON.mockReturnValue(Promise.reject(error));
      API.delete(<<<placeHolder>>>s[0]).then(function() {
        expect(Dispatcher.dispatch).toBeCalledWith({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'delete'
        });
        done();
      });
    });
  });
});
