// The fetch function is a browser window global. It is not an export.
// This polyfill is required for fetch to work on Safari and IE
require('whatwg-fetch');

var JSON_HEADERS = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
};

function _checkStatus(response) {
  return response.ok ? response : response.json().then(function(errors) {
    for (var error in errors) { throw new Error(errors[error]); }
  });
}

function _performHttpRequest(url, method, body, headers) {
  if (url.lastIndexOf('http', 0) === -1) {
    url = window.baseUrl + url;
  }

  return fetch(url, {
    method: method,
    body: body,
    headers: headers,
    credentials: 'same-origin'
  }).then(_checkStatus);
}

function _performJsonHttpRequest(url, method, body) {
  return _performHttpRequest(url, method, JSON.stringify(body), JSON_HEADERS)
  .then(_checkStatus)
  .then(function(response) { return response.json(); });
}

module.exports = {

  getQueryStringValue: function(key) {
    return unescape(window.location.search.replace(
      new RegExp('^(?:.*[&\\?]' + escape(key).replace(/[\.\+\*]/g, '\\$&') +
        '(?:\\=([^&]*))?)?.*$', 'i'), '$1')
    );
  },

  get: function(url, body) {
    return _performHttpRequest(url, 'GET', body, {});
  },

  put: function(url, body) {
    return _performHttpRequest(url, 'PUT', body, {});
  },

  post: function(url, body) {
    return _performHttpRequest(url, 'POST', body, {});
  },

  delete: function(url, body) {
    return _performHttpRequest(url, 'DELETE', body, {});
  },

  getJSON: function(url, body) {
    return _performJsonHttpRequest(url, 'GET', body);
  },

  putJSON: function(url, body) {
    return _performJsonHttpRequest(url, 'PUT', body);
  },

  postJSON: function(url, body) {
    return _performJsonHttpRequest(url, 'POST', body);
  },

  deleteJSON: function(url, body) {
    return _performJsonHttpRequest(url, 'DELETE', body);
  }

};
