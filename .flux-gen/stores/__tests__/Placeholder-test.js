'use strict'

jest.dontMock('../<<<Placeholder>>>');

describe('<<<Placeholder>>>', function() {

  var Store;
  var <<<Placeholder>>>;
  var Dispatcher;
  var callback;
  var actionReceive<<<Placeholder>>>s;
  var actionReceive<<<Placeholder>>>sError;
  var actionCreate<<<Placeholder>>>;
  var actionCreate<<<Placeholder>>>Error;
  var actionUpdate<<<Placeholder>>>;
  var actionUpdate<<<Placeholder>>>Error;
  var actionDelete<<<Placeholder>>>;
  var actionDelete<<<Placeholder>>>Error;
  var Constants = require('../../constants/Constants');

  beforeEach(function() {
    Store = require('../<<<Placeholder>>>');
    Store.emit = jest.genMockFunction();
    window.<<<Placeholder>>> = { refresh: jest.fn(), deleteAll: jest.fn() };
    Dispatcher = require('../../dispatcher/Dispatcher');
    callback = Dispatcher.register.mock.calls[0][0];

    actionReceive<<<Placeholder>>>s = {
      type: Constants.RECEIVE_<<<PLACEHOLDER>>>S,
      <<<placeholder>>>s: [ { id: 1 } ]
    };
    actionReceive<<<Placeholder>>>sError = {
      type: Constants.RECEIVE_<<<PLACEHOLDER>>>S_ERROR,
      error: 'bagpipes'
    };

    actionCreate<<<Placeholder>>> = {
      type: Constants.CREATE_<<<PLACEHOLDER>>>,
      <<<placeholder>>>: { id: 1, email: 'Rhiannon@fleetwood.mac' }
    };
    actionCreate<<<Placeholder>>>Error = {
      type: Constants.CREATE_<<<PLACEHOLDER>>>_ERROR,
      error: 'bagpipes'
    };

    actionUpdate<<<Placeholder>>> = {
      type: Constants.UPDATE_<<<PLACEHOLDER>>>,
      <<<placeholder>>>: { id: 1, email: 'Rhiannon@fleetwood.mac' }
    };
    actionUpdate<<<Placeholder>>>Error = {
      type: Constants.UPDATE_<<<PLACEHOLDER>>>_ERROR,
      error: 'bagpipes'
    };

    actionDelete<<<Placeholder>>> = {
      type: Constants.DELETE_<<<PLACEHOLDER>>>,
      <<<placeholder>>>Id: 1
    };
    actionDelete<<<Placeholder>>>Error = {
      type: Constants.DELETE_<<<PLACEHOLDER>>>_ERROR,
      error: 'bagpipes'
    };
  });

  it('registers a callback with the dispatcher', function() {
    expect(Dispatcher.register.mock.calls.length).toBe(1);
  });

  describe('RECEIVE_<<<PLACEHOLDER>>>S', function() {
    beforeEach(function() { callback(actionReceive<<<Placeholder>>>s); });

    it('populates the store', function() {
      expect(Store.all().length).toBe(3);
    });

    it('emits a CHANGE_EVENT', function() {
      expect(Store.emit).toBeCalledWith('CHANGE');
    });
  });

  describe('CREATE_<<<PLACEHOLDER>>>', function() {
    beforeEach(function() { callback(actionCreate<<<Placeholder>>>); });

    it('adds a <<<placeholder>>>', function() {
      expect(Store.all().length).toBe(1);
    });

    it('emits a CREATE_EVENT', function() {
      expect(Store.emit).toBeCalledWith('CREATE', actionCreate<<<Placeholder>>>.<<<placeholder>>>);
    });

    it('emits a CHANGE_EVENT', function() {
      expect(Store.emit).toBeCalledWith('CHANGE');
    });
  });

  describe('UPDATE_<<<PLACEHOLDER>>>', function() {
    beforeEach(function() {
      callback(actionReceive<<<Placeholder>>>s);
      callback(actionUpdate<<<Placeholder>>>);
    });

    it('updates an <<<placeholder>>>', function() {
      expect(Store.find(1).email).toBe(actionUpdate<<<Placeholder>>>.<<<placeholder>>>.email);
    });

    it('emits a UPDATE_EVENT', function() {
      expect(Store.emit).toBeCalledWith('UPDATE', actionUpdate<<<Placeholder>>>.<<<placeholder>>>);
    });

    it('emits a CHANGE_EVENT', function() {
      expect(Store.emit).toBeCalledWith('CHANGE');
    });
  });

  describe('DELETE_<<<PLACEHOLDER>>>', function() {
    beforeEach(function() {
      callback(actionReceive<<<Placeholder>>>s);
      callback(actionDelete<<<Placeholder>>>);
    });

    it('deletes an <<<placeholder>>>', function() {
      expect(Store.find(actionDelete<<<Placeholder>>>.<<<placeholder>>>Id)).toBeUndefined();
    });

    it('emits a CHANGE_EVENT', function() {
      expect(Store.emit).toBeCalledWith('CHANGE');
    });
  });

  describe('RECEIVE_<<<PLACEHOLDER>>>S_ERROR', function() {
    it('emits an ERROR event', function() {
      callback(actionReceive<<<Placeholder>>>sError); 
      expect(Store.emit).toBeCalledWith('ERROR', actionReceive<<<Placeholder>>>sError.error);
    });
  });

  describe('CREATE_<<<PLACEHOLDER>>>_ERROR', function() {
    it('emits an ERROR event', function() {
      callback(actionCreate<<<Placeholder>>>Error); 
      expect(Store.emit).toBeCalledWith('ERROR', actionCreate<<<Placeholder>>>Error.error);
    });
  });

  describe('UPDATE_<<<PLACEHOLDER>>>_ERROR', function() {
    it('emits an ERROR event', function() {
      callback(actionUpdate<<<Placeholder>>>Error);
      expect(Store.emit).toBeCalledWith('ERROR', actionUpdate<<<Placeholder>>>Error.error);
    });
  });

  describe('<<<PLACEHOLDER>>>_API_REQUEST_STARTING', function() {
    describe('getAll', function() {
      it('emits an <<<PLACEHOLDER>>>_API_GET_ALL_STARTING event', function() {
        callback({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_STARTING,
          requestType: 'getAll',
        });
        expect(Store.emit).toBeCalledWith(Constants.<<<PLACEHOLDER>>>_API_GET_ALL_STARTING);
      });
    });

    describe('create', function() {
      it('emits an <<<PLACEHOLDER>>>_API_CREATE_STARTING event', function() {
        callback({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_STARTING,
          requestType: 'create',
        });
        expect(Store.emit).toBeCalledWith(Constants.<<<PLACEHOLDER>>>_API_CREATE_STARTING);
      });
    });

    describe('update', function() {
      it('emits an <<<PLACEHOLDER>>>_API_UPDATE_STARTING event', function() {
        callback({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_STARTING,
          requestType: 'update',
        });
        expect(Store.emit).toBeCalledWith(Constants.<<<PLACEHOLDER>>>_API_UPDATE_STARTING);
      });
    });

    describe('delete', function() {
      it('emits an <<<PLACEHOLDER>>>_API_DELETE_STARTING event', function() {
        callback({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_STARTING,
          requestType: 'delete',
        });
        expect(Store.emit).toBeCalledWith(Constants.<<<PLACEHOLDER>>>_API_DELETE_STARTING);
      });
    });
  });

  describe('<<<PLACEHOLDER>>>_API_REQUEST_DONE', function() {
    describe('getAll', function() {
      it('emits an <<<PLACEHOLDER>>>_API_GET_ALL_DONE event', function() {
        callback({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'getAll',
        });
        expect(Store.emit).toBeCalledWith(Constants.<<<PLACEHOLDER>>>_API_GET_ALL_DONE);
      });
    });

    describe('create', function() {
      it('emits an <<<PLACEHOLDER>>>_API_CREATE_DONE event', function() {
        callback({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'create',
        });
        expect(Store.emit).toBeCalledWith(Constants.<<<PLACEHOLDER>>>_API_CREATE_DONE);
      });
    });

    describe('update', function() {
      it('emits an <<<PLACEHOLDER>>>_API_UPDATE_DONE event', function() {
        callback({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'update',
        });
        expect(Store.emit).toBeCalledWith(Constants.<<<PLACEHOLDER>>>_API_UPDATE_DONE);
      });
    });

    describe('delete', function() {
      it('emits an <<<PLACEHOLDER>>>_API_DELETE_DONE event', function() {
        callback({
          type: Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE,
          requestType: 'delete',
        });
        expect(Store.emit).toBeCalledWith(Constants.<<<PLACEHOLDER>>>_API_DELETE_DONE);
      });
    });
  });

  describe('DELETE_<<<PLACEHOLDER>>>_ERROR', function() {
    it('emits an ERROR event', function() {
      callback(actionDelete<<<Placeholder>>>Error);
      expect(Store.emit).toBeCalledWith('ERROR', actionDelete<<<Placeholder>>>Error.error);
    });
  });

  describe('FETCH_GOOGLE_<<<PLACEHOLDER>>>S_ERROR', function() {
    it('emits an ERROR event', function() {
      callback(actionFetchGoogle<<<Placeholder>>>sError);
      expect(Store.emit).toBeCalledWith('ERROR', actionFetchGoogle<<<Placeholder>>>sError.error);
    });
  });

  describe('IMPORT_CSV_<<<PLACEHOLDER>>>S_ERROR', function() {
    it('emits an ERROR event', function() {
      callback(actionImportCsv<<<Placeholder>>>sError);
      expect(Store.emit).toBeCalledWith('ERROR', actionImportCsv<<<Placeholder>>>sError.error);
    });
  });

  describe('IMPORT_VCARD_<<<PLACEHOLDER>>>S_ERROR', function() {
    it('emits an ERROR event', function() {
      callback(actionImportVcard<<<Placeholder>>>sError);
      expect(Store.emit).toBeCalledWith('ERROR', actionImportVcard<<<Placeholder>>>sError.error);
    });
  });

  describe('<<<Placeholder>>>.all', function() {
    it('returns an empty array if the store is empty', function() {
      expect(Store.all()).toEqual([]);
    });

    it('returns an array of <<<Placeholder>>>s', function() {
      callback(actionReceive<<<Placeholder>>>s);
      expect(Store.all().length).toBe(3);
    });
  });

  describe('<<<Placeholder>>>.find', function() {
    it('returns undefined if it cant find a match', function() {
      expect(Store.find(232)).toEqual(undefined);
    });

    it('can find an <<<placeholder>>> by ID', function() {
      callback(actionCreate<<<Placeholder>>>);
      var id = actionCreate<<<Placeholder>>>.<<<placeholder>>>.id
      expect(Store.find(id)).toEqual(actionCreate<<<Placeholder>>>.<<<placeholder>>>);
    });
  });

  describe('<<<Placeholder>>>.off', function() {
    it('removes an event listener', function() {
      var callback = jest.fn();
      Store.removeListener = jest.fn();
      Store.off('bla', callback);
      expect(Store.removeListener).toBeCalledWith('bla', callback);
    })
  });
});
