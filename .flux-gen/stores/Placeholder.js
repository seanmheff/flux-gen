var Dispatcher = require('../dispatcher/Dispatcher');
var EventEmitter = require('events').EventEmitter;
var Constants = require('../constants/Constants');
var assign = require('object-assign');
var _ = require('lodash');


var _<<<placeHolder>>>s = [];

var _update<<<Placeholder>>>s = function(<<<placeHolder>>>s) {
  // First, we sort them by id - in lower case
  <<<placeHolder>>>s = _.sortBy(<<<placeHolder>>>s, function(<<<placeHolder>>>) {
    return <<<placeHolder>>>.id();
  });

  // Then we add any additional properties we need and return the array
  return _.map(<<<placeHolder>>>s, function(<<<placeHolder>>>) {
    return <<<placeHolder>>>;
  });
};


var <<<Placeholder>>>s = assign({}, EventEmitter.prototype, {

  all: function() {
    return _<<<placeHolder>>>s;
  },

  find: function(id) {
    return _.find(_<<<placeHolder>>>s, { id: parseInt(id) });
  },

  off: function(eventName, callback) {
    this.removeListener(eventName, callback);
  }

});


Dispatcher.register(function(action) {

  switch(action.type) {

    case Constants.RECEIVE_<<<PLACEHOLDER>>>S:
      _<<<placeHolder>>>s = _update<<<Placeholder>>>s(action.<<<placeHolder>>>s);
      _loaded = true;
      <<<Placeholder>>>s.emit('CHANGE');
      break;

    case Constants.CREATE_<<<PLACEHOLDER>>>:
      _<<<placeHolder>>>s = _update<<<Placeholder>>>s(_<<<placeHolder>>>s.concat([action.<<<placeHolder>>>]));
      <<<Placeholder>>>s.emit('CHANGE');
      <<<Placeholder>>>s.emit('CREATE', action.<<<placeHolder>>>);
      break;

    case Constants.UPDATE_<<<PLACEHOLDER>>>:
      _.remove(_<<<placeHolder>>>s, { 'id': action.<<<placeHolder>>>.id });
      _<<<placeHolder>>>s = _update<<<Placeholder>>>s(_<<<placeHolder>>>s.concat([action.<<<placeHolder>>>]));
      <<<Placeholder>>>s.emit('CHANGE');
      <<<Placeholder>>>s.emit('UPDATE', action.<<<placeHolder>>>);
      break;

    case Constants.DELETE_<<<PLACEHOLDER>>>:
      _.remove(_<<<placeHolder>>>s, { 'id': action.<<<placeHolder>>>Id });
      <<<Placeholder>>>s.emit('CHANGE');
      <<<Placeholder>>>s.emit('DESTROY');
      break;

    case Constants.RECEIVE_<<<PLACEHOLDER>>>S_ERROR:
    case Constants.CREATE_<<<PLACEHOLDER>>>_ERROR:
    case Constants.UPDATE_<<<PLACEHOLDER>>>_ERROR:
    case Constants.DELETE_<<<PLACEHOLDER>>>_ERROR:
      <<<Placeholder>>>s.emit('ERROR', action.error);
      break;

    case Constants.<<<PLACEHOLDER>>>_API_REQUEST_STARTING:
      switch(action.requestType) {
        case 'getAll':
          <<<Placeholder>>>s.emit(Constants.<<<PLACEHOLDER>>>_API_GET_ALL_STARTING);
          break;
        case 'create':
          <<<Placeholder>>>s.emit(Constants.<<<PLACEHOLDER>>>_API_CREATE_STARTING);
          break;
        case 'update':
          <<<Placeholder>>>s.emit(Constants.<<<PLACEHOLDER>>>_API_UPDATE_STARTING);
          break;
        case 'delete':
          <<<Placeholder>>>s.emit(Constants.<<<PLACEHOLDER>>>_API_DELETE_STARTING);
          break;
      }
      break;

    case Constants.<<<PLACEHOLDER>>>_API_REQUEST_DONE:
      switch(action.requestType) {
        case 'getAll':
          <<<Placeholder>>>s.emit(Constants.<<<PLACEHOLDER>>>_API_GET_ALL_DONE);
          break;
        case 'create':
          <<<Placeholder>>>s.emit(Constants.<<<PLACEHOLDER>>>_API_CREATE_DONE);
          break;
        case 'update':
          <<<Placeholder>>>s.emit(Constants.<<<PLACEHOLDER>>>_API_UPDATE_DONE);
          break;
        case 'delete':
          <<<Placeholder>>>s.emit(Constants.<<<PLACEHOLDER>>>_API_DELETE_DONE);
          break;
      }
      break;
  }

});

module.exports = <<<Placeholder>>>s;
