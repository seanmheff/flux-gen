'use strict';

jest.dontMock('../<<<Placeholder>>>ServerActionCreators');

describe('<<<Placeholder>>>ServerActionCreators', function() {
  var Action;
  var Constants;
  var Dispatcher;

  beforeEach(function(){
    Action = require('../<<<Placeholder>>>ServerActionCreators');
    Constants = require('../../constants/Constants');
    Dispatcher = require('../../dispatcher/Dispatcher');
  });

  describe('receive<<<Placeholder>>>s', function() {
    it('dispatches an RECEIVE_<<<PLACEHOLDER>>>S event', function() {
      var <<<placeHolder>>>s = [ { id: 1 } ];
      Action.receive<<<Placeholder>>>s(<<<placeHolder>>>s);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.RECEIVE_<<<PLACEHOLDER>>>S,
        <<<placeHolder>>>s: <<<placeHolder>>>s
      });
    });
  });

  describe('receive<<<Placeholder>>>sError', function() {
    it('dispatches an RECEIVE_<<<PLACEHOLDER>>>S_ERROR event', function() {
      var error = 'error';
      Action.receive<<<Placeholder>>>sError(error);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.RECEIVE_<<<PLACEHOLDER>>>S_ERROR,
        error: error
      });
    });
  });

  describe('create<<<Placeholder>>>', function() {
    it('dispatches an CREATE_<<<PLACEHOLDER>>> event', function() {
      var <<<placeHolder>>> = { id: 1 };
      Action.create<<<Placeholder>>>(<<<placeHolder>>>);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.CREATE_<<<PLACEHOLDER>>>,
        <<<placeHolder>>>: <<<placeHolder>>>
      });
    });
  });

  describe('create<<<Placeholder>>>Error', function() {
    it('dispatches an CREATE_<<<PLACEHOLDER>>>_ERROR event', function() {
      var error = 'error';
      Action.create<<<Placeholder>>>Error(error);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.CREATE_<<<PLACEHOLDER>>>_ERROR,
        error: error
      });
    });
  });

  describe('update<<<Placeholder>>>', function() {
    it('dispatches an UPDATE_<<<PLACEHOLDER>>> event', function() {
      var <<<placeholder>>> = { id: 1 };
      Action.update<<<Placeholder>>>(<<<placeHolder>>>);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.UPDATE_<<<PLACEHOLDER>>>,
        <<<placeHolder>>>: <<<placeHolder>>>
      });
    });
  });

  describe('update<<<Placeholder>>>Error', function() {
    it('dispatches an UPDATE_<<<PLACEHOLDER>>>_ERROR event', function() {
      var error = 'error';
      Action.update<<<Placeholder>>>Error(error);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.UPDATE_<<<PLACEHOLDER>>>_ERROR,
        error: error
      });
    });
  });

  describe('delete<<<Placeholder>>>', function() {
    it('dispatches an DELETE_<<<PLACEHOLDER>>> event', function() {
      var <<<placeholder>>>Id = 1;
      Action.delete<<<Placeholder>>>(<<<placeHolder>>>Id);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.DELETE_<<<PLACEHOLDER>>>,
        <<<placeHolder>>>Id: <<<placeHolder>>>Id
      });
    });
  });

  describe('delete<<<Placeholder>>>Error', function() {
    it('dispatches an DELETE_<<<PLACEHOLDER>>>_ERROR event', function() {
      var error = 'error';
      Action.delete<<<Placeholder>>>Error(error);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.DELETE_<<<PLACEHOLDER>>>_ERROR,
        error: error
      });
    });
  });
});
