'use strict';

jest.dontMock('../<<<Placeholder>>>ActionCreators');

describe('<<<Placeholder>>>ActionCreators', function() {
  var API;
  var Store;
  var Action;
  var Constants;
  var Dispatcher;
  var <<<placeHolder>>>;

  beforeEach(function() {
    API = require('../../api/<<<Placeholder>>>');
    Store = require('../../stores/<<<Placeholder>>>');
    Action = require('../<<<Placeholder>>>ActionCreators');
    Constants = require('../../constants/Constants');
    Dispatcher = require('../../dispatcher/Dispatcher');
    <<<placeHolder>>> = { id: 1, email: 'fleewood@mac.rocks' };
  });

  describe('getAll', function() {
    it('calls the <<<Placeholder>>> API to get all <<<placeHolder>>>s', function() {
      Action.getAll();
      expect(API.getAll).toBeCalled();
    });
  });

  describe('create', function() {
    beforeEach(function() {
      Store.find = jest.fn();
      Store.find.mockReturnValue(null);
    });

    it('calls API create when valid <<<placeHolder>>>', function() {
      Action.create(<<<placeHolder>>>);
      expect(API.create).toBeCalledWith(<<<placeHolder>>>);
    });

    it('dispatches an error if "id" is not provided', function() {
      <<<placeHolder>>>.id = null;
      Action.create(<<<placeHolder>>>);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.CREATE_<<<PLACEHOLDER>>>_ERROR,
        error: 'A <<<placeHolder>>> must have an id'
      });
    });
  });

  describe('update', function() {
    it('calls API update when valid <<<placeholder>>>', function() {
      Action.update(<<<placeHolder>>>);
      expect(API.update).toBeCalledWith(<<<placeHolder>>>);
    });

    it('dispatches an error if "id" is not provided', function() {
      <<<placeHolder>>>.id = null;
      Action.update(<<<placeHolder>>>);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.UPDATE_<<<PLACEHOLDER>>>_ERROR,
        error: 'A <<<placeHolder>>> must have an id'
      });
    });
  });

  describe('delete', function() {
    it('calls the <<<Placeholder>>> API to delete <<<placeHolder>>>', function() {
      Action.delete(<<<placeHolder>>>);
      expect(API.delete).toBeCalledWith(<<<placeHolder>>>);
    });

    it('dispatches an error if "id" is not provided', function() {
      <<<placeHolder>>>.id = null;
      Action.update(<<<placeHolder>>>);
      expect(Dispatcher.dispatch).toBeCalledWith({
        type: Constants.DELETE<<<PLACEHOLDER>>>_ERROR,
        error: 'A <<<placeHolder>>> must have an id'
      });
    });
  });
});
