var API = require('../api/<<<Placeholder>>>');
var <<<Placeholder>>>Store = require('../stores/<<<Placeholder>>>');
var Constants = require('../constants/Constants');
var Dispatcher = require('../dispatcher/Dispatcher');

function _validateCommon(<<<placeHolder>>>) {
  if (!<<<placeholder>>>.id) { return 'A <<<placeholder>>> must have a id. '; }
}

function _validateCreate(<<<placeHolder>>>) {
  var error = _validateCommon(<<<placeHolder>>>);
  if (error) { return error; }
  if (<<<Placeholder>>>Store.find(<<<placeHolder>>>.id)) {
    return 'You already have a <<<placeholder>>> with this id.';
  }
}

function _validateUpdate(<<<placeHolder>>>) {
  var error = _validateCommon(<<<placeHolder>>>);
  if (error) { return error; }
}

module.exports = {

  getAll: function() {
    API.getAll();
  },

  create: function(<<<placeHolder>>>) {
    var error = _validateCreate(<<<placeHolder>>>);
    if (error) {
      Dispatcher.dispatch({
        type: Constants.CREATE_<<<PLACEHOLDER>>>_ERROR,
        error: error
      });
    } else {
      API.create(<<<placeHolder>>>);
    }
  },

  update: function(<<<placeHolder>>>) {
    var error = _validateUpdate(<<<placeHolder>>>);
    if (error) {
      Dispatcher.dispatch({
        type: Constants.UPDATE_<<<PLACEHOLDER>>>_ERROR,
        error: error
      });
    } else {
      API.update(<<<placeHolder>>>);
    }
  },

  delete: function(<<<placeHolder>>>) {
    API.delete(<<<placeHolder>>>);
  }

};
