var Constants = require('../constants/Constants');
var Dispatcher = require('../dispatcher/Dispatcher');

module.exports = {

  receive<<<Placeholder>>>: function(<<<placeHolder>>>) {
    Dispatcher.dispatch({
      type: Constants.RECEIVE_<<<PLACEHOLDER>>>,
      <<<placeholder>>>: <<<placeholder>>>
    });
  },

  receive<<<Placeholder>>>Error: function(error) {
    Dispatcher.dispatch({
      type: Constants.RECEIVE_<<<PLACEHOLDER>>>_ERROR,
      error: error
    });
  },

  create<<<Placeholder>>>: function(<<<placeHolder>>>) {
    Dispatcher.dispatch({
      type: Constants.CREATE_<<<PLACEHOLDER>>>,
      <<<placeholder>>>: <<<placeholder>>>
    });
  },

  create<<<Placeholder>>>Error: function(error) {
    Dispatcher.dispatch({
      type: Constants.CREATE_<<<PLACEHOLDER>>>_ERROR,
      error: error
    });
  },

  update<<<Placeholder>>>: function(<<<placeHolder>>>) {
    Dispatcher.dispatch({
      type: Constants.UPDATE_<<<PLACEHOLDER>>>,
      <<<placeholder>>>: <<<placeholder>>>
    });
  },

  update<<<Placeholder>>>Error: function(error) {
    Dispatcher.dispatch({
      type: Constants.UPDATE_<<<PLACEHOLDER>>>_ERROR,
      error: error
    });
  },

  delete<<<Placeholder>>>: function(<<<placeHolder>>>Id) {
    Dispatcher.dispatch({
      type: Constants.DELETE_<<<PLACEHOLDER>>>,
      <<<placeholder>>>Id: <<<placeholder>>>Id
    });
  },

  delete<<<Placeholder>>>Error: function(error) {
    Dispatcher.dispatch({
      type: Constants.DELETE_<<<PLACEHOLDER>>>_ERROR,
      error: error
    });
  }

};
