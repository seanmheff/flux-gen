import os
import re

namespace = ".flux-gen/"
name = raw_input('Please enter a model name: ');
joe_1 = name[0] + re.sub(r"([A-Z])", r"_\1", name[1:])
joe = name[0] + re.sub(r"([A-Z])", r"_\1", name[1:]).upper()

files = [
        { "path": "actions/",           "name": 'PlaceholderActionCreators.js' },
        { "path": "actions/",           "name": 'PlaceholderServerActionCreators.js' },
        { "path": "actions/__tests__/", "name": 'PlaceholderActionCreators-test.js' },
        { "path": "actions/__tests__/", "name": 'PlaceholderServerActionCreators-test.js' },
        { "path": "api/",               "name": 'Placeholder.js' },
        { "path": "api/__tests__/",     "name": 'Placeholder-test.js' },
        { "path": "stores/",            "name": 'Placeholder.js' },
        { "path": "stores/__tests__/",  "name": 'Placeholder-test.js' }
]
for f in files:
    if not os.path.exists(f['path']):
        os.makedirs(f['path'])

    full_filename = os.path.join(os.path.dirname(__file__), namespace, f['path'], f['name'])
    new_filename = full_filename.replace(namespace, '')
    new_filename = new_filename.replace('Placeholder', name)

    if os.path.isfile(new_filename):
        print new_filename + " exists. Skipping..."
        continue

    with open(new_filename, 'w+') as fout:
        print "Creating " + new_filename[2:]
        with open(full_filename, "rt") as fin:
            for line in fin:
                out = line.replace('<<<PLACEHOLDER>>>', joe.upper())
                out = out.replace('<<<Placeholder>>>', name)
                out = out.replace('<<<placeHolder>>>', name[0].lower() + name[1:])
                out = out.replace('<<<placeholder>>>', name.lower())
                fout.write(out)
